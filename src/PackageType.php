<?php

declare(strict_types=1);

namespace SlyFoxCreative\PackageManagers;

enum PackageType
{
    case Production;

    case Development;
}
