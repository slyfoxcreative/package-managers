<?php

declare(strict_types=1);

namespace SlyFoxCreative\PackageManagers;

use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Mindscreen\YarnLock\YarnLock;
use Symfony\Component\Yaml\Yaml;

class YarnSet extends PackageSet
{
    private Collection $packageJson;
    private Collection $currentVersions;

    /**
     * Returns the key where production dependencies can be found in the
     * package manager file.
     */
    protected function productionKey(): string
    {
        return 'dependencies';
    }

    /**
     * Returns the key where development dependencies can be found in the
     * package manager file.
     */
    protected function developmentKey(): string
    {
        return 'devDependencies';
    }

    /**
     * Returns a list of names and constraints for packges of the given type.
     *
     * @param PackageType $type The package type (production or development)
     */
    protected function packages(PackageType $type): Collection
    {
        $key = $this->key($type);

        $this->packageJson ??= loadJson($this->filePath);

        if (isset($this->packageJson[$key])) {
            return collect($this->packageJson[$key])
                ->map(fn ($c, $p) => ['name' => $p, 'constraint' => $c])
                ->values()
            ;
        }

        return new Collection();
    }

    /**
     * Returns the current installed version for the given package.
     *
     * @param string $name The package's name
     */
    protected function currentVersion(string $name): array
    {
        if (!isset($this->currentVersions)) {
            $lines = file($this->lockPath);
            if ($lines[1] === "# yarn lockfile v1\n") {
                $this->currentVersions = $this->fetchCurrentCurrentVersionsForYarn1();
            } else {
                $this->currentVersions = $this->fetchCurrentCurrentVersionsForYarn2();
            }
        }

        return [$this->currentVersions[$name], ''];
    }

    /**
     * Returns the latest version in the remote repository for the
     * given package.
     *
     * @param string $name The package's name
     */
    protected function latestVersion(string $name): array
    {
        $url = 'https://api.npms.io/v2/package/' . urlencode($name);

        try {
            $response = $this->client->get($url);
            $status = $response->getStatusCode();
        } catch (RequestException $exception) {
            if ($exception->hasResponse()) {
                $status = $exception->getResponse()->getStatusCode();
            }
        }

        if ($status === 404) {
            return ['?', 'Not found in the repository'];
        }

        if ($status !== 200) {
            return ['?', 'Failed to fetch data from API'];
        }

        try {
            $json = json_decode((string) $response->getBody(), true, 512, JSON_THROW_ON_ERROR);
        } catch (\JsonException $exception) {
            return ['?', 'Failed to parse data from API'];
        }

        $version = data_get($json, 'collected.metadata.version');

        if (is_null($version)) {
            return ['?', 'Missing version data'];
        }

        $version = $this->parser->normalize($version);

        $error = isset($json['error']) ? 'API reports an error parsing metadata' : '';

        return [$version, $error];
    }

    /**
     * Fetches current installed versions for packages from a Yarn 1 lock file.
     */
    private function fetchCurrentCurrentVersionsForYarn1(): Collection
    {
        $lockfile = YarnLock::fromString(file_get_contents($this->lockPath));

        return collect($lockfile->getPackages())->mapWithKeys(function ($package) {
            try {
                return [$package->getName() => $this->parser->normalize($package->getVersion())];
            } catch (\UnexpectedValueException $e) {
                return [$package->getName() => $package->getVersion()];
            }
        });
    }

    /**
     * Fetches current installed versions for packages from a Yarn 2 or later
     * lock file.
     */
    private function fetchCurrentCurrentVersionsForYarn2(): Collection
    {
        return collect(Yaml::parseFile($this->lockPath))
            ->reject(fn ($v, $k) => $k === '__metadata')
            ->mapWithKeys(function ($data, $key) {
                $key = (string) Str::of($key)->before(':')->beforeLast('@');

                try {
                    return [$key => $this->parser->normalize($data['version'])];
                } catch (\UnexpectedValueException $e) {
                    return [$key => $data['version']];
                }
            })
        ;
    }
}
