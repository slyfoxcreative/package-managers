<?php

declare(strict_types=1);

namespace SlyFoxCreative\PackageManagers;

use Composer\Semver\Comparator;
use Illuminate\Support\Collection;

/**
 * A package managed by a package manager.
 */
class Package
{
    private string $name;
    private string $constraint;
    private string $currentVersion;
    private string $latestVersion;
    private Collection $errors;
    private string $outdated;

    /**
     * Constructs new a Package object.
     *
     * @param string     $name           The package name
     * @param string     $constraint     The package's version constraint
     * @param string     $currentVersion The current installed version
     * @param string     $latestVersion  The latest available version
     * @param Collection $errors         The list of errors generated while
     *                                   getting the package data
     */
    public function __construct(
        string $name,
        string $constraint,
        string $currentVersion,
        string $latestVersion,
        Collection $errors
    ) {
        $this->name = $name;
        $this->constraint = $constraint;
        $this->currentVersion = $currentVersion;
        $this->latestVersion = $latestVersion;
        $this->errors = $errors;
    }

    /**
     * Returns an array of package data.
     */
    public function toArray(): array
    {
        return [
            $this->name,
            $this->constraint,
            $this->currentVersion,
            $this->latestVersion,
            $this->outdated(),
            $this->errors(),
        ];
    }

    /**
     * Returns 'X' if the package is outdated and '' if it is up-to-date or
     * the outdated status cannot be determined.
     */
    private function outdated(): string
    {
        if ($this->currentVersion === '?' || $this->latestVersion === '?') {
            return '';
        }

        return Comparator::greaterThan($this->latestVersion, $this->currentVersion) ? 'X' : '';
    }

    /**
     * Returns a string of the errors separated by newlines.
     */
    private function errors(): string
    {
        return trim($this->errors->join("\n"));
    }
}
