<?php

declare(strict_types=1);

namespace SlyFoxCreative\PackageManagers;

use Composer\Semver\Semver;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class ComposerSet extends PackageSet
{
    private Collection $composerJson;
    private Collection $composerLock;
    private Collection $packages;
    private Collection $currentVersions;

    /**
     * Returns the key where production dependencies can be found in the
     * package manager file.
     */
    protected function productionKey(): string
    {
        return 'require';
    }

    /**
     * Returns the key where development dependencies can be found in the
     * package manager file.
     */
    protected function developmentKey(): string
    {
        return 'require-dev';
    }

    /**
     * Returns a list of names and constraints for packges of the given type.
     *
     * @param PackageType $type The package type (production or development)
     */
    protected function packages(PackageType $type): Collection
    {
        $key = $this->key($type);

        $this->packages ??= new Collection();

        if (!isset($this->packages[$key])) {
            if (isset($this->composerJson()[$key])) {
                $this->packages[$key] = collect($this->composerJson()[$key])
                    ->filter(fn ($c, $p) => preg_match('/[^\/]+\/[^\/]+/', $p))
                    ->reject(fn ($c, $p) => Str::startsWith($c, 'dev-'))
                    ->map(fn ($c, $p) => ['name' => $p, 'constraint' => $c])
                    ->values()
                ;
            } else {
                $this->packages[$key] = new Collection();
            }
        }

        return $this->packages[$key];
    }

    /**
     * Returns the current installed version for the given package.
     *
     * @param string $name The package's name
     */
    protected function currentVersion(string $name): array
    {
        if (!isset($this->currentVersions)) {
            $json = $this->composerLock();
            $packages = collect($json['packages'])->concat($json['packages-dev']);

            $this->currentVersions = $packages->mapWithKeys(
                fn ($p) => [$p['name'] => $this->parser->normalize($p['version'])]
            );
        }

        if (!$this->currentVersions->has($name)) {
            $replacement = $this->replacements()
                ->filter(fn ($r, $p) => $r->contains($name))
                ->keys()
                ->first()
            ;

            $error = !is_null($replacement)
                ? "Replaced by {$replacement}"
                : 'Missing in composer.lock';

            return ['?', $error];
        }

        return [$this->currentVersions[$name], ''];
    }

    /**
     * Returns the latest version in the remote repository for the
     * given package.
     *
     * @param string $name The package's name
     */
    protected function latestVersion(string $name): array
    {
        $response = null;
        $status = null;
        foreach ($this->repositories() as $domain) {
            $url = "https://{$domain}/p2/{$name}.json";

            try {
                $response = $this->client->get($url);
                $status = $response->getStatusCode();
            } catch (RequestException $exception) {
                if ($exception->hasResponse()) {
                    $status = $exception->getResponse()->getStatusCode();
                }
            }

            if ($status !== 404) {
                break;
            }
        }

        if ($status === 404) {
            return ['?', 'Not found in any repositories'];
        }

        if ($status !== 200) {
            return ['?', 'Failed to fetch data from API'];
        }

        try {
            $json = json_decode((string) $response->getBody(), true, 512, JSON_THROW_ON_ERROR);
        } catch (\JsonException $exception) {
            return ['?', 'Failed to parse data from API'];
        }

        $packages = collect(data_get($json, "packages.{$name}"));
        $versions = $packages->map->version_normalized->toArray();
        $version = collect(Semver::sort($versions))->last();

        if (is_null($version)) {
            return ['?', 'Missing version data'];
        }

        return [$version, ''];
    }

    /**
     * Returns the JSON data from composer.json.
     */
    private function composerJson(): Collection
    {
        return $this->composerJson ??= loadJson($this->filePath);
    }

    private function composerLock(): Collection
    {
        return $this->composerLock ??= loadJson($this->lockPath);
    }

    /**
     * Returns the list of repository domains defined in composer.json.
     */
    private function repositories(): Collection
    {
        $repositories = collect($this->composerJson()['repositories'] ?? []);

        $domains = $repositories
            ->map->url
            ->map(fn ($url) => parse_url($url, PHP_URL_HOST))
        ;

        if (!$domains->contains('repo.packagist.org')) {
            $domains->prepend('repo.packagist.org');
        }

        return $domains;
    }

    /**
     * Returns the list of packages that replace other packages.
     */
    private function replacements(): Collection
    {
        $json = $this->composerLock();

        return collect($json['packages'])->concat($json['packages-dev'])
            ->mapWithKeys(fn ($p) => [$p['name'] => data_get($p, 'replace')])
            ->filter()
            ->map(fn ($a) => collect($a)->keys())
        ;
    }
}
