<?php

declare(strict_types=1);

namespace SlyFoxCreative\PackageManagers\Commands;

use SlyFoxCreative\PackageManagers\PackageManager;
use SlyFoxCreative\PackageManagers\PackageType;

/**
 * Common methods for versions commands.
 */
trait CommonMethods
{
    /**
     * Checks that a file exists and is readable and shows an error message
     * if not.
     */
    private function checkFiles(...$paths): bool
    {
        $error = false;

        foreach ($paths as $path) {
            if (!file_exists($path) || !is_readable($path)) {
                $this->error("Can't read {$path}");
                $error = true;
            }
        }

        return $error;
    }

    /**
     * Shows a table of data for a set of packages.
     *
     * @param string         $title   The table's title
     * @param PackageManager $manager The package manager to get packages from
     * @param PackageType    $type    The package type (production or development)
     */
    private function packagesTable(string $title, PackageManager $manager, PackageType $type): void
    {
        if ($manager->packageCount($type) === 0) {
            return;
        }

        $this->info("{$title}...");
        $bar = $this->output->createProgressBar($manager->packageCount($type));
        $packages = $manager->packages($type, fn () => $bar->advance())->map->toArray();
        $bar->finish();
        $this->newLine();
        $this->table(['Package', 'Constraint', 'Current', 'Latest', 'Outdated', 'Error'], $packages);
        $this->newLine();
    }
}
