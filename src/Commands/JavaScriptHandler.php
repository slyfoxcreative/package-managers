<?php

declare(strict_types=1);

namespace SlyFoxCreative\PackageManagers\Commands;

use SlyFoxCreative\PackageManagers\PackageManager;
use SlyFoxCreative\PackageManagers\PackageType;
use SlyFoxCreative\PackageManagers\YarnSet;

trait JavaScriptHandler
{
    use CommonMethods;

    public function handle()
    {
        $file = $this->option('file');
        $lockfile = $this->option('lockfile');

        if ($this->checkFiles($file, $lockfile)) {
            return 1;
        }

        $manager = new PackageManager(new YarnSet($file, $lockfile));

        $this->newLine();
        $this->packagesTable('Packages', $manager, PackageType::Production);
        $this->packagesTable('Development Packages', $manager, PackageType::Development);
    }
}
