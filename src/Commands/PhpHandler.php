<?php

declare(strict_types=1);

namespace SlyFoxCreative\PackageManagers\Commands;

use SlyFoxCreative\PackageManagers\ComposerSet;
use SlyFoxCreative\PackageManagers\PackageManager;
use SlyFoxCreative\PackageManagers\PackageType;

trait PhpHandler
{
    use CommonMethods;

    public function handle()
    {
        $file = $this->option('file');
        $lockfile = $this->option('lockfile');

        if ($this->checkFiles($file, $lockfile)) {
            return 1;
        }

        $manager = new PackageManager(new ComposerSet($file, $lockfile));

        $this->newLine();
        $this->packagesTable('Packages', $manager, PackageType::Production);
        $this->packagesTable('Development Packages', $manager, PackageType::Development);
    }
}
