<?php

declare(strict_types=1);

namespace SlyFoxCreative\PackageManagers;

use Illuminate\Support\Collection;

/**
 * Load JSON data from a file and return it as a Collection.
 */
function loadJson(string $path): Collection
{
    if (!file_exists($path) || !is_readable($path)) {
        throw new \RuntimeException("Can't read file {$path}");
    }

    return collect(json_decode(file_get_contents($path), true, 512, JSON_THROW_ON_ERROR));
}
