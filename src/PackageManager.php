<?php

declare(strict_types=1);

namespace SlyFoxCreative\PackageManagers;

use Illuminate\Support\Collection;

/**
 * Version data about a set of packages managed by a packager manager.
 */
class PackageManager
{
    private PackageSet $set;

    public function __construct(PackageSet $set)
    {
        $this->set = $set;
    }

    /**
     * Returns the number of packages managed by the package manager.
     *
     * @param PackageType $type The package type (production or development)
     */
    public function packageCount(PackageType $type): int
    {
        return $this->set->names($type)->count();
    }

    /**
     * Returns a Collection of Packages of the given type.
     *
     * @param PackageType $type  The package type (production or development)
     * @param callable    $rider A closure to do some extra work while getting
     *                           the list of packages, such as updating a
     *                           progress bar
     */
    public function packages(PackageType $type, ?callable $rider = null): Collection
    {
        return $this->set->names($type)
            ->map(function ($name) use ($rider) {
                $package = $this->set->package($name);

                if (!is_null($rider)) {
                    $rider();
                }

                return $package;
            })
        ;
    }
}
