<?php

declare(strict_types=1);

namespace SlyFoxCreative\PackageManagers\Tests\Commands;

use Illuminate\Console\Command;
use SlyFoxCreative\PackageManagers\Commands\JavaScriptHandler;

class TestJavaScriptCommand extends Command
{
    use JavaScriptHandler;

    protected $signature = 'test:javascript {--file=} {--lockfile=}';

    protected $description = 'A JavaScript test command';
}
