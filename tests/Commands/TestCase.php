<?php

declare(strict_types=1);

namespace SlyFoxCreative\PackageManagers\Tests\Commands;

use Orchestra\Testbench\TestCase as BaseTestCase;
use SlyFoxCreative\PackageManagers\Tests\TestMethods;

class TestCase extends BaseTestCase
{
    use TestMethods;

    protected function getPackageProviders($app)
    {
        return [
            TestServiceProvider::class,
        ];
    }
}
