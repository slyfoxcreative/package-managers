<?php

declare(strict_types=1);

namespace SlyFoxCreative\PackageManagers\Tests\Commands;

use Illuminate\Console\Command;
use SlyFoxCreative\PackageManagers\Commands\PhpHandler;

class TestPhpCommand extends Command
{
    use PhpHandler;

    protected $signature = 'test:php {--file=} {--lockfile=}';

    protected $description = 'A PHP test command';
}
