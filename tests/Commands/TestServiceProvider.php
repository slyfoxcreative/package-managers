<?php

declare(strict_types=1);

namespace SlyFoxCreative\PackageManagers\Tests\Commands;

use Illuminate\Support\ServiceProvider;

class TestServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->commands([
            TestJavaScriptCommand::class,
            TestPhpCommand::class,
        ]);
    }
}
