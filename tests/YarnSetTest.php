<?php

declare(strict_types=1);

namespace SlyFoxCreative\PackageManagers\Tests;

use GuzzleHttp\Psr7\Response;
use SlyFoxCreative\PackageManagers\Package;
use SlyFoxCreative\PackageManagers\PackageSet;
use SlyFoxCreative\PackageManagers\PackageType;
use SlyFoxCreative\PackageManagers\YarnSet;

class YarnSetTest extends TestCase
{
    public function testNames()
    {
        $set = new YarnSet(
            $this->fixturesPath('package.json'),
            $this->fixturesPath('yarn1.lock'),
        );

        $this->assertEquals(
            collect([
                'bootstrap',
                'jquery',
                'popper.js',
                'stimulus',
            ]),
            $set->names(PackageType::Production),
        );
    }

    public function testPackageWithYarn1()
    {
        $client = $this->mockGuzzleClient(
            // https://api.npms.io/v2/package/jquery
            new Response(200, [], $this->fixture('jquery.json')),
        );

        PackageSet::setHttpClient($client);

        $set = new YarnSet(
            $this->fixturesPath('package.json'),
            $this->fixturesPath('yarn1.lock'),
        );

        $this->assertEquals(
            new Package('jquery', '^3.6', '3.6.0.0', '3.6.0.0', collect()),
            $set->package('jquery'),
        );
    }

    public function testPackageWithYarn2()
    {
        $client = $this->mockGuzzleClient(
            // https://api.npms.io/v2/package/jquery
            new Response(200, [], $this->fixture('jquery.json')),
        );

        PackageSet::setHttpClient($client);

        $set = new YarnSet(
            $this->fixturesPath('package.json'),
            $this->fixturesPath('yarn2.lock'),
        );

        $this->assertEquals(
            new Package('jquery', '^3.6', '3.6.0.0', '3.6.0.0', collect()),
            $set->package('jquery'),
        );
    }

    public function testPackageWithMissingPackage()
    {
        $client = $this->mockGuzzleClient(
            // https://api.npms.io/v2/package/jquery
            new Response(404, [], 'Not found'),
            // https://api.npms.io/v2/package/jquery
            new Response(404, [], 'Not found'),
        );

        PackageSet::setHttpClient($client);

        $set = new YarnSet(
            $this->fixturesPath('package.json'),
            $this->fixturesPath('yarn1.lock'),
        );

        $this->assertEquals(
            new Package('jquery', '^3.6', '3.6.0.0', '?', collect(['Not found in the repository'])),
            $set->package('jquery'),
        );
    }

    public function testPackageWithHttpError()
    {
        $client = $this->mockGuzzleClient(
            // https://api.npms.io/v2/package/jquery
            new Response(500, [], 'Server error'),
        );

        PackageSet::setHttpClient($client);

        $set = new YarnSet(
            $this->fixturesPath('package.json'),
            $this->fixturesPath('yarn1.lock'),
        );

        $this->assertEquals(
            new Package('jquery', '^3.6', '3.6.0.0', '?', collect(['Failed to fetch data from API'])),
            $set->package('jquery'),
        );
    }

    public function testPackageWithJsonError()
    {
        $client = $this->mockGuzzleClient(
            // https://api.npms.io/v2/package/jquery
            new Response(200, [], $this->fixture('invalid.json')),
        );

        PackageSet::setHttpClient($client);

        $set = new YarnSet(
            $this->fixturesPath('package.json'),
            $this->fixturesPath('yarn1.lock'),
        );

        $this->assertEquals(
            new Package('jquery', '^3.6', '3.6.0.0', '?', collect(['Failed to parse data from API'])),
            $set->package('jquery'),
        );
    }

    public function testPackageWithMissingVersionData()
    {
        $client = $this->mockGuzzleClient(
            // https://api.npms.io/v2/package/jquery
            new Response(200, [], $this->fixture('test.json')),
        );

        PackageSet::setHttpClient($client);

        $set = new YarnSet(
            $this->fixturesPath('package.json'),
            $this->fixturesPath('yarn1.lock'),
        );

        $this->assertEquals(
            new Package('jquery', '^3.6', '3.6.0.0', '?', collect(['Missing version data'])),
            $set->package('jquery'),
        );
    }

    public function testPackageWithMetadataParseError()
    {
        $client = $this->mockGuzzleClient(
            // https://api.npms.io/v2/package/bootstrap
            new Response(200, [], $this->fixture('bootstrap.json')),
        );

        PackageSet::setHttpClient($client);

        $set = new YarnSet(
            $this->fixturesPath('package.json'),
            $this->fixturesPath('yarn1.lock'),
        );

        $this->assertEquals(
            new Package('bootstrap', '^4.6', '4.6.0.0', '5.1.0.0', collect(['API reports an error parsing metadata'])),
            $set->package('bootstrap'),
        );
    }
}
