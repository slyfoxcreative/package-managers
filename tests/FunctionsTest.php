<?php

declare(strict_types=1);

namespace SlyFoxCreative\PackageManagers\Tests;

use function SlyFoxCreative\PackageManagers\loadJson;

class FunctionsTest extends TestCase
{
    public function testLoadJson()
    {
        $json = loadJson($this->fixturesPath('test.json'));

        $this->assertEquals(collect(['test' => 1]), $json);
    }

    public function testLoadJsonWithMissingFile()
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage("Can't read file missing.json");

        loadJson('missing.json');
    }

    public function testLoadJsonWithNonJsonFile()
    {
        $this->expectException(\JsonException::class);

        loadJson($this->fixturesPath('invalid.json'));
    }
}
