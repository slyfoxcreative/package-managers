<?php

declare(strict_types=1);

namespace SlyFoxCreative\PackageManagers\Tests;

use GuzzleHttp\Psr7\Response;
use SlyFoxCreative\PackageManagers\ComposerSet;
use SlyFoxCreative\PackageManagers\Package;
use SlyFoxCreative\PackageManagers\PackageManager;
use SlyFoxCreative\PackageManagers\PackageSet;
use SlyFoxCreative\PackageManagers\PackageType;

class PackageManagerTest extends TestCase
{
    public function testPackageCount()
    {
        $manager = new PackageManager(new ComposerSet(
            $this->fixturesPath('composer.json'),
            $this->fixturesPath('composer.lock'),
        ));

        $this->assertSame(6, $manager->packageCount(PackageType::Production));
    }

    public function testPackages()
    {
        $client = $this->mockGuzzleClient(
            // https://repo.packagist.org/p2/composer/semver.json
            new Response(200, [], $this->fixture('composer-semver.json')),
            // https://repo.packagist.org/p2/guzzlehttp/guzzle.json
            new Response(200, [], $this->fixture('guzzlehttp-guzzle.json')),
            // https://repo.packagist.org/p2/illuminate/support.json
            new Response(200, [], $this->fixture('illuminate-support.json')),
            // https://repo.packagist.org/p2/laravel/framework.json
            new Response(200, [], $this->fixture('laravel-framework.json')),
            // https://repo.packagist.org/p2/psr/log.json
            new Response(200, [], $this->fixture('psr-log.json')),
            // https://repo.packagist.org/p2/slyfoxcreative/coding-styles.json
            new Response(404, [], 'Not found'),
            // https://composer.slyfoxcreative.com/p2/slyfoxcreative/coding-styles.json
            new Response(200, [], $this->fixture('slyfoxcreative-coding-styles.json')),
        );

        PackageSet::setHttpClient($client);

        $manager = new PackageManager(new ComposerSet(
            $this->fixturesPath('composer.json'),
            $this->fixturesPath('composer.lock'),
        ));

        $riderObject = (object) ['called' => false];
        $rider = fn () => $riderObject->called = true;

        $expected = collect([
            new Package('composer/semver', '^3.2', '3.2.5.0', '3.2.5.0', collect()),
            new Package('guzzlehttp/guzzle', '^7.3', '7.3.0.0', '7.3.0.0', collect()),
            new Package('illuminate/support', '^8.54', '?', '8.54.0.0', collect(['Replaced by laravel/framework'])),
            new Package('laravel/framework', '^8.54', '8.54.0.0', '8.54.0.0', collect()),
            new Package('psr/log', '^1.0', '1.1.4.0', '3.0.0.0', collect()),
            new Package('slyfoxcreative/coding-styles', '^1.2', '1.2.0.0', '1.2.0.0', collect()),
        ]);

        $packages = $manager->packages(PackageType::Production, $rider);

        $this->assertEquals($expected, $packages);
        $this->assertTrue($riderObject->called);
    }
}
