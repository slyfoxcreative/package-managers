<?php

declare(strict_types=1);

namespace SlyFoxCreative\PackageManagers\Tests;

use GuzzleHttp\Psr7\Response;
use SlyFoxCreative\PackageManagers\ComposerSet;
use SlyFoxCreative\PackageManagers\Package;
use SlyFoxCreative\PackageManagers\PackageSet;
use SlyFoxCreative\PackageManagers\PackageType;

class ComposerSetTest extends TestCase
{
    public function testNames()
    {
        $set = new ComposerSet(
            $this->fixturesPath('composer.json'),
            $this->fixturesPath('composer.lock'),
        );

        $this->assertEquals(
            collect([
                'composer/semver',
                'guzzlehttp/guzzle',
                'illuminate/support',
                'laravel/framework',
                'psr/log',
                'slyfoxcreative/coding-styles',
            ]),
            $set->names(PackageType::Production),
        );
    }

    public function testPackage()
    {
        $client = $this->mockGuzzleClient(
            // https://repo.packagist.org/p2/composer/semver.json
            new Response(200, [], $this->fixture('composer-semver.json')),
        );

        PackageSet::setHttpClient($client);

        $set = new ComposerSet(
            $this->fixturesPath('composer.json'),
            $this->fixturesPath('composer.lock'),
        );

        $this->assertEquals(
            new Package('composer/semver', '^3.2', '3.2.5.0', '3.2.5.0', collect()),
            $set->package('composer/semver'),
        );
    }

    public function testPackageWithMissingPackage()
    {
        $client = $this->mockGuzzleClient(
            // https://repo.packagist.org/p2/composer/semver.json
            new Response(404, [], 'Not found'),
            // https://composer.slyfoxcreative.com/p2/composer/semver.json
            new Response(404, [], 'Not found'),
        );

        PackageSet::setHttpClient($client);

        $set = new ComposerSet(
            $this->fixturesPath('composer.json'),
            $this->fixturesPath('composer.lock'),
        );

        $this->assertEquals(
            new Package('composer/semver', '^3.2', '3.2.5.0', '?', collect(['Not found in any repositories'])),
            $set->package('composer/semver'),
        );
    }

    public function testPackageWithHttpError()
    {
        $client = $this->mockGuzzleClient(
            // https://repo.packagist.org/p2/composer/semver.json
            new Response(500, [], 'Server error'),
        );

        PackageSet::setHttpClient($client);

        $set = new ComposerSet(
            $this->fixturesPath('composer.json'),
            $this->fixturesPath('composer.lock'),
        );

        $this->assertEquals(
            new Package('composer/semver', '^3.2', '3.2.5.0', '?', collect(['Failed to fetch data from API'])),
            $set->package('composer/semver'),
        );
    }

    public function testPackageWithJsonError()
    {
        $client = $this->mockGuzzleClient(
            // https://repo.packagist.org/p2/composer/semver.json
            new Response(200, [], $this->fixture('invalid.json')),
        );

        PackageSet::setHttpClient($client);

        $set = new ComposerSet(
            $this->fixturesPath('composer.json'),
            $this->fixturesPath('composer.lock'),
        );

        $this->assertEquals(
            new Package('composer/semver', '^3.2', '3.2.5.0', '?', collect(['Failed to parse data from API'])),
            $set->package('composer/semver'),
        );
    }

    public function testPackageWithMissingVersionData()
    {
        $client = $this->mockGuzzleClient(
            // https://repo.packagist.org/p2/composer/semver.json
            new Response(200, [], $this->fixture('test.json')),
        );

        PackageSet::setHttpClient($client);

        $set = new ComposerSet(
            $this->fixturesPath('composer.json'),
            $this->fixturesPath('composer.lock'),
        );

        $this->assertEquals(
            new Package('composer/semver', '^3.2', '3.2.5.0', '?', collect(['Missing version data'])),
            $set->package('composer/semver'),
        );
    }

    public function testPackageWithReplacement()
    {
        $client = $this->mockGuzzleClient(
            // https://repo.packagist.org/p2/illuminate/support.json
            new Response(200, [], $this->fixture('illuminate-support.json')),
        );

        PackageSet::setHttpClient($client);

        $set = new ComposerSet(
            $this->fixturesPath('composer.json'),
            $this->fixturesPath('composer.lock'),
        );

        $this->assertEquals(
            new Package('illuminate/support', '^8.54', '?', '8.54.0.0', collect(['Replaced by laravel/framework'])),
            $set->package('illuminate/support'),
        );
    }

    public function testWithNoRepositoriesInComposerJson()
    {
        $client = $this->mockGuzzleClient(
            // https://repo.packagist.org/p2/composer/semver.json
            new Response(200, [], $this->fixture('composer-semver.json')),
        );

        PackageSet::setHttpClient($client);

        $set = new ComposerSet(
            $this->fixturesPath('norepositories.json'),
            $this->fixturesPath('composer.lock'),
        );

        $this->assertEquals(
            new Package('composer/semver', '^3.2', '3.2.5.0', '3.2.5.0', collect()),
            $set->package('composer/semver'),
        );
    }
}
