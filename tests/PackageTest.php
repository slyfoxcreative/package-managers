<?php

declare(strict_types=1);

namespace SlyFoxCreative\PackageManagers\Tests;

use SlyFoxCreative\PackageManagers\Package;

class PackageTest extends TestCase
{
    public function testToArray()
    {
        $package = new Package('test', '^1.0', '1.0.0', '1.0.0', collect(['error 1', 'error 2']));

        $this->assertSame(
            ['test', '^1.0', '1.0.0', '1.0.0', '', "error 1\nerror 2"],
            $package->toArray(),
        );
    }

    public function testOutdated()
    {
        $package = new Package('test', '^1.0', '1.0.0', '1.1.0', collect());

        $this->assertSame(
            ['test', '^1.0', '1.0.0', '1.1.0', 'X', ''],
            $package->toArray(),
        );
    }
}
