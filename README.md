# Package Managers

Library for getting data about packages managed by Composer and Yarn.

The `Commands\PhpHandler` and `Commands\JavaScriptHandler` traits can
be used in Laravel and Laravel Zero to make commands that show tables
of package version data.
